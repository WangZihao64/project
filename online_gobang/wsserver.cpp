#include<iostream>
#include<string>
#include<websocketpp/server.hpp>
//非ssl/tls加密的一个asio框架
#include<websocketpp/config/asio_no_tls.hpp>
typedef websocketpp::server<websocketpp::config::asio> wsserver_t;
void open_callback(wsserver_t* srv,websocketpp::connection_hdl hdl)
{
    std::cout << "websocket握手成功" << std::endl;
}
void close_callback(wsserver_t* srv,websocketpp::connection_hdl hdl)
{
    std::cout << "websocket连接断开 " << std::endl;
}
void message_callback(wsserver_t* srv,websocketpp::connection_hdl hdl,wsserver_t::message_ptr msg)
{
    //获取客户端发送的数据
    std::cout << "wsmsg: " << msg->get_payload() << std::endl;
    std::string rsp="Client say: " + msg->get_payload();
    wsserver_t::connection_ptr con=srv->get_con_from_hdl(hdl);
    con->send(rsp,websocketpp::frame::opcode::text);
}
void http_callback(wsserver_t* srv,websocketpp::connection_hdl hdl)
{
    //给客户端返回一个hello world页面
    wsserver_t::connection_ptr con=srv->get_con_from_hdl(hdl);
    std::cout << "body: " << con->get_request_body() << std::endl;
    websocketpp::http::parser::request req=con->get_request();
    std::cout << "method: " << req.get_method() << std::endl;
    std::cout << "uri: " << req.get_uri() << std::endl;
    //设置响应码
    con->set_status(websocketpp::http::status_code::ok);
    //添加http响应头部字段
    con->append_header("Content-Type","text/html");
    //设置http响应正⽂
    std::string body="<html><body><h1>Hello World</h1></body></html>";
    con->set_body(body);
}
int main()
{
    //1.实例化server对象
    wsserver_t wssrv;
    //2.设置打印日志
    wssrv.set_access_channels(websocketpp::log::alevel::none); //禁止打印
    //3.初始化asio调度器
    wssrv.init_asio();
    //设置地址重用
    wssrv.set_reuse_addr(true);
    //4.设置回调函数
    //4.1 请求回调处理函数
    wssrv.set_http_handler(bind(http_callback,&wssrv,std::placeholders::_1));
    //4.2 握手成功回调处理函数
    wssrv.set_open_handler(bind(open_callback,&wssrv,std::placeholders::_1));
    //4.3 连接关闭回调处理函数
    wssrv.set_close_handler(bind(close_callback,&wssrv,std::placeholders::_1));
    //4.4 消息回调处理函数
    wssrv.set_message_handler(bind(message_callback,&wssrv,std::placeholders::_1,std::placeholders::_2));
    //5.设置监听窗口
    wssrv.listen(8081);
    //6.开始获取新连接
    wssrv.start_accept();
    //7.启动服务器
    wssrv.run();
    return 0;
}
