#include<iostream>
#include<jsoncpp/json/json.h>
#include<string>
#include<sstream>
std::string serialize()
{
    //1.将需要序列化的数据，存储在Value对象中
    Json::Value root;
    root["姓名"]="王子豪";
    root["年龄"]=18;
    root["成绩"].append(90);
    root["成绩"].append(80);
    root["成绩"].append(95);
    //2.实例化一个StreamWriterBuilder工厂类对象
    Json::StreamWriterBuilder swb;
    //3.通过StreamWriterBuilder工厂类对象生产一个StreamWriter对象
    Json::StreamWriter* sw=swb.newStreamWriter();
    //4.使用StreamWriter对象，对Value对象中存储的数据进行序列化
    std::stringstream ss;
    sw->write(root,&ss);
    delete sw;
    
    return ss.str();
}
void deserialize(const std::string& str)
{
    //1.实例化一个CharReaderBuilder工厂类对象
    Json::CharReaderBuilder crb;
    //2.通过CharReaderBuilder工厂类对象生产一个CharReader对象
    Json::CharReader* cr=crb.newCharReader();
    //3.定义一个Value对象，存放解析后的数据
    Json::Value root;
    std::string err;
    //4.使用CharReader对象进行json字符串格式str的反序列化
    cr->parse(str.c_str(),str.c_str()+str.size(),&root,&err);
    //5.逐个元素去访问
    std::cout << "姓名" << root["姓名"].asString() << std::endl;
    std::cout << "年龄" << root["年龄"].asInt() << std::endl;
    int sz=root["成绩"].size();
    for(int i=0;i<sz;++i)
    {
        std::cout << root["成绩"][i].asFloat() << " ";
    }
    std::cout << std::endl;
}
int main()
{
    std::string s=serialize();
    std::cout << s << std::endl;
    deserialize(s);
}