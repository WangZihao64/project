#ifndef LOGGER_H
#define LOGGER_H
#include<iostream>
#include <time.h>
#define INF 0
#define DBG 1
#define ERR 2
#define DEFAULT_LEVEL INF
#define LOG(level,format,...) do{\
    if(DEFAULT_LEVEL>level) break;\
    time_t t=time(NULL);\
    struct tm* lt=localtime(&t);\
    char buffer[30]={0};\
    strftime(buffer,sizeof(buffer)-1,"%H:%M:%S",lt);\
    fprintf(stdout,"[%s %s:%d]" format "\n",buffer,__FILE__,__LINE__,##__VA_ARGS__);\
}while(0);
#define ILOG(format,...) LOG(INF,format,##__VA_ARGS__)
#define DLOG(format,...) LOG(DBG,format,##__VA_ARGS__)
#define ELOG(format,...) LOG(ERR,format,##__VA_ARGS__)
#endif