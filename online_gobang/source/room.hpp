#ifndef ROOM_H
#define ROOM_H
#include "util.hpp"
#include "db.hpp"
#include "online.hpp"
#include <vector>
#define ROW 15
#define COL 15
#define CHESS_WHITE 1
#define CHESS_BLACK 2
typedef enum
{
    GAME_START,
    GAME_OVER
} room_state;
class room
{
private:
    // 房间id
    uint64_t _room_id;
    // 房间状态(决定一个玩家退出房间所做的动作)
    room_state _state;
    // 玩家数量(决定了房间什么时候销毁)
    int _player_count;
    // 白棋方id
    int _white_user_id;
    int _black_user_id;
    // 用户信息表的句柄，胜利和失败时更新用户数据
    user_table *_tb_user;
    // 在线用户管理句柄
    online_manager *_online_user;
    // 棋盘
    std::vector<std::vector<int>> _board;
    bool five(int row, int col, int row_off, int col_off, int color)
    {
        int count = 1;
        int search_row = row + row_off;
        int search_col = col + col_off;
        DLOG("棋子数量%d",count);
        while (search_row >= 0 && search_row < ROW && search_col >= 0 && search_col < COL && _board[search_row][search_col] == color)
        {
            ++count;
            search_row += row_off;
            search_col += col_off;
        }
        search_row = row - row_off;
        search_col = col - col_off;
        DLOG("棋子数量%d",count);
        while (search_row >= 0 && search_row < ROW && search_col >= 0 && search_col < COL && _board[search_row][search_col] == color)
        {
            ++count;
            search_row -= row_off;
            search_col -= col_off;
        }
        DLOG("棋子数量%d",count);
        return count >= 5;
    }
    // 返回0表示没有胜利者
    uint64_t check_win(int chess_row, int chess_col, int color)
    {
        if (five(chess_row, chess_col, 0, 1, color) || five(chess_row, chess_col, 1, 0, color) || five(chess_row, chess_col, -1, -1, color) || five(chess_row, chess_col, -1, 1, color))
        {
            return color == CHESS_WHITE ? _white_user_id : _black_user_id;
        }
        return 0;
    }

public:
    room(uint64_t room_id, user_table *tb_user, online_manager *online_user)
        : _room_id(room_id),
          _state(GAME_START),
          _player_count(0),
          _tb_user(tb_user),
          _online_user(online_user),
          _board(ROW, std::vector<int>(COL, 0))
    {
        DLOG("%lu 房间创建成功", _room_id);
    }
    ~room()
    {
        DLOG("%lu 房间销毁成功", _room_id);
    }
    uint64_t id()
    {
        return _room_id;
    }
    room_state state()
    {
        return _state;
    }
    int play_count()
    {
        return _player_count;
    }
    void add_white_user(uint64_t uid)
    {
        _white_user_id = uid;
        ++_player_count;
    }
    void add_black_user(uint64_t uid)
    {
        _black_user_id = uid;
        ++_player_count;
    }
    uint64_t get_white_user()
    {
        return _white_user_id;
    }
    uint64_t get_black_user()
    {
        return _black_user_id;
    }
    // 处理下棋动作
    Json::Value handle_chess(Json::Value &req)
    {
        Json::Value resp = req;
        // 2.两个用户是否都是在线的
        uint64_t cur_uid = req["uid"].asUInt64();
        int chess_row = req["row"].asInt();
        int chess_col = req["col"].asInt();
        if (_online_user->is_in_game_room(_white_user_id) == false)
        {
            DLOG("白棋玩家掉线");
            resp["result"] = true;
            resp["reason"] = "运气真好！对方掉线，不战而胜！";
            resp["winner"] = (Json::UInt64)_black_user_id;
            return resp;
        }
        if (_online_user->is_in_game_room(_black_user_id) == false)
        {
            DLOG("黑棋玩家掉线");
            resp["result"] = true;
            resp["reason"] = "运气真好！对方掉线，不战而胜！";
            resp["winner"] = (Json::UInt64)_white_user_id;
            return resp;
        }
        // 3.获取下棋的位置，判断当前走棋是否合理
        if (_board[chess_row][chess_col] != 0)
        {
            DLOG("有其他棋子");
            resp["result"] = false;
            resp["reason"] = "当前位置已经有其他棋子！";
            return resp;
        }
        int cur_color = cur_uid == _white_user_id ? CHESS_WHITE : CHESS_BLACK;
        _board[chess_row][chess_col] = cur_color;
        // 4.判断是否有玩家胜利（从当前走棋位置开始判断）
        uint64_t winner_id = check_win(chess_row, chess_col, cur_color);
        if (winner_id != 0)
        {
            DLOG("有玩家胜利");
            resp["reason"] = "五星连珠，战无敌！";
        }
        resp["optype"] = "put_chess";
        resp["result"] = true;
        resp["winner"] = (Json::UInt64)winner_id;
        return resp;
    }
    // 处理聊天
    Json::Value handle_chat(Json::Value &req)
    {
        Json::Value resp = req;
        std::string msg = req["message"].asString();
        // 检测是否有敏感词
        if (msg.find("垃圾") != std::string::npos)
        {
            resp["result"] = false;
            resp["reason"] = "消息中包含敏感词，不能发送";
            return resp;
        }
        // 广播消息--返回消息
        resp["result"] = true;
        return resp;
    }
    // 处理玩家退出动作
    void handle_exit(uint64_t uid)
    {
        Json::Value resp;
        // 如果是下棋中退出，则对方胜利，否则下棋结束了退出，则正常退出
        if (_state == GAME_START)
        {
            resp["optype"] = "put_chess";
            resp["result"] = true;
            resp["reason"] = "对方掉线不战而胜";
            resp["room_id"] = (Json::UInt64)_room_id;
            resp["uid"] = (Json::UInt64)uid;
            resp["row"] = -1;
            resp["col"] = -1;
            uint64_t winner = uid == _white_user_id ? _black_user_id : _white_user_id;
            resp["winner"] = (Json::UInt64)winner;
            uint64_t loser_id = winner == _white_user_id ? _black_user_id : _white_user_id;
            _tb_user->win(winner);
            _tb_user->lose(loser_id);
            _state = GAME_OVER;
        }
        broadcast(resp);
        --_player_count;
    }
    // 总的请求处理，在函数内部区别请求类型，根据请求类型调用不同的处理请求，得到响应进行广播
    void handle_request(Json::Value &req)
    {
        // 校验房间号是否匹配
        Json::Value resp;
        uint64_t room_id = req["room_id"].asUInt64();
        if (room_id != _room_id)
        {
            resp["optype"] = req["optype"].asString();
            resp["result"] = false;
            resp["reason"] = "房间号不匹配";
            broadcast(resp);
            return;
        }
        // 根据不同类型，调用不同的处理函数
        if (req["optype"].asString() == "put_chess")
        {
            resp = handle_chess(req);
            if (resp["winner"].asUInt64() != 0)
            {
                uint64_t winner_id = resp["winner"].asUInt64();
                // 更新数据库
                uint64_t loser_id = winner_id == _white_user_id ? _black_user_id : _white_user_id;
                _tb_user->win(winner_id);
                _tb_user->lose(loser_id);
                _state = GAME_OVER;
            }
        }
        else if (req["optype"].asString() == "chat")
        {
            resp = handle_chat(req);
        }
        else
        {
            resp["optype"] = req["optype"].asString();
            resp["result"] = true;
            resp["reason"] = "未知请求类型";
        }
        broadcast(resp);
    }
    // 将指定信息广播给房间中的所有玩家
    void broadcast(Json::Value &resp)
    {
        //对要响应的信息进行序列化
        std::string body;
        json_util::serialize(resp, body);
        //获取房间中所有用户的通信连接
        wsserver_t::connection_ptr wconn = _online_user->get_conn_from_room(_white_user_id);
        if (wconn.get() != nullptr)
        {
            wconn->send(body);
        }
        wsserver_t::connection_ptr bconn = _online_user->get_conn_from_room(_black_user_id);
        if (bconn.get() != nullptr)
        {
            bconn->send(body);
        }
    }
};
using room_ptr = std::shared_ptr<room>;
class room_manager
{
private:
    //房间ID分配计数器
    uint64_t _next_rid;
    std::mutex _mutex;
    user_table *_user_table;
    online_manager *_online_user;
    //房间ID和房间信息的映射
    std::unordered_map<uint64_t, room_ptr> _rooms;
    //房间ID和用户ID的映射
    std::unordered_map<uint64_t, uint64_t> _users;

public:
    //初始化房间ID计数器
    room_manager(user_table *user_table, online_manager *online_user)
        : _next_rid(1),
          _user_table(user_table),
          _online_user(online_user)
    {
        DLOG("房间管理模块初始化完毕！");
    }
    ~room_manager()
    {
        DLOG("房间管理模块即将销毁！");
    }
    //为两个用户创建房间，并返回房间的智能指针管理对象
    room_ptr create_room(uint64_t uid1, uint64_t uid2)
    {
        // 1.校验两个用户是否都还在游戏大厅中，只有都在才需要创建房间
        if (_online_user->is_in_game_hall(uid1) == false)
        {
            DLOG("用户:%lu 不在大厅中，创建房间失败！", uid1);
            return room_ptr();
        }
        if (_online_user->is_in_game_hall(uid2) == false)
        {
            DLOG("用户:%lu 不在大厅中，创建房间失败！", uid2);
            return room_ptr();
        }
        // 2.创建房间，将信息添加到房间中
        std::unique_lock<std::mutex> lock(_mutex);
        room_ptr pt(new room(_next_rid, _user_table, _online_user));
        pt->add_white_user(uid1);
        pt->add_black_user(uid2);
        // 3.哈希表映射
        _rooms.insert(std::make_pair(_next_rid, pt));
        _users.insert(std::make_pair(uid1, _next_rid));
        _users.insert(std::make_pair(uid2, _next_rid));
        ++_next_rid;
        // 4.返回房间信息
        return pt;
    }
    //通过房间ID获取房间信息
    room_ptr get_room_by_rid(uint64_t rid)
    {
        std::unique_lock<std::mutex> lock(_mutex);
        auto it = _rooms.find(rid);
        if (it == _rooms.end())
        {
            return room_ptr();
        }
        return it->second;
    }
    //通过用户ID获取房间信息
    room_ptr get_room_by_uid(uint64_t uid)
    {
        std::unique_lock<std::mutex> lock(_mutex);
        auto uit = _users.find(uid);
        if (uit == _users.end())
        {
            return room_ptr();
        }
        auto it = _rooms.find(uit->second);
        if (it == _rooms.end())
        {
            return room_ptr();
        }
        return it->second;
    }
    //通过房间ID销毁房间
    void remove_room(uint64_t rid)
    {
        //房间信息是通过shared_ptr在_rooms中进行管理的，因此只要将shared_ptr从_rooms中移除即可
        // 1.通过房间ID，获取房间信息
        room_ptr pt = get_room_by_rid(rid);
        if (pt.get() == nullptr)
        {
            return;
        }
        //获取房间中的用户信息
        uint64_t uid1 = pt->get_white_user();
        uint64_t uid2 = pt->get_black_user();
        //移除房间管理中的用户信息
        std::unique_lock<std::mutex> lock(_mutex);
        _users.erase(uid1);
        _users.erase(uid2);
        //移除房间管理信息
        _rooms.erase(rid);
    }
    //删除房间中的指定用户，如果房间中没有用户了，销毁房间
    void remove_room_user(uint64_t uid)
    {
        room_ptr pt = get_room_by_uid(uid);
        if (pt.get() == nullptr)
        {
            return;
        }
        //处理玩家退出房间动作
        pt->handle_exit(uid);
        if (pt->play_count() == 0)
        {
            remove_room(pt->id());
        }
    }
};
#endif