#ifndef SERVER_H
#define SERVER_H
#include "logger.hpp"
#include "matcher.hpp"
#include "online.hpp"
#include "room.hpp"
#include "session.hpp"
#include "util.hpp"
#define WWWROOT "./wwwroot/"
class gobang_server
{
private:
    std::string _web_root;
    wsserver_t _wssrv;
    user_table _ut;
    online_manager _om;
    room_manager _rm;
    session_manager _sm;
    matcher _mm;
    void http_resp(wsserver_t::connection_ptr &conn, bool result, websocketpp::http::status_code::value code, const std::string &reason)
    {
        Json::Value resp;
        resp["result"] = result;
        resp["reason"] = reason;
        std::string body;
        json_util::serialize(resp, body);
        //这里使用这些接口，但是如果是websocket协议是使用send
        conn->set_status(code);
        conn->set_body(body);
        conn->append_header("Content-Type", "application/json");
    }
    void file_handler(wsserver_t::connection_ptr &conn)
    {
        //静态资源请求处理
        websocketpp::http::parser::request req = conn->get_request();
        std::string method = req.get_method();
        std::string uri = req.get_uri();
        std::string pathname = _web_root + uri;
        if (pathname.back() == '/')
        {
            pathname += "login.html";
        }
        std::string body;
        bool ret = file_util::read(pathname, body);
        if (ret == false)
        {
            body += "<html>";
            body += "<head>";
            body += "<meta charset='UTF-8'/>";
            body += "</head>";
            body += "<body>";
            body += "<h1> Not Found </h1>";
            body += "</body>";
            conn->set_status(websocketpp::http::status_code::not_found);
            conn->set_body(body);
            return;
        }
        conn->set_status(websocketpp::http::status_code::ok);
        conn->set_body(body);
    }
    void reg(wsserver_t::connection_ptr &conn)
    {
        //用户注册功能请求处理
        // 1.获取请求正文
        std::string req = conn->get_request_body();
        // 2.对正文进行json反序列化，得到用户名和密码
        Json::Value login_info;
        Json::Value resp;
        bool ret = json_util::deserialize(req, login_info);
        if (ret == false)
        {
            DLOG("反序列化注册信息失败");
            return http_resp(conn, false, websocketpp::http::status_code::bad_request, "请求正文格式错误");
        }
        // 3.数据库新增
        if (login_info["username"].isNull() || login_info["password"].isNull())
        {
            DLOG("用户名密码不完整");
            return http_resp(conn, false, websocketpp::http::status_code::bad_request, "请输入用户名/密码");
        }
        ret = _ut.insert(login_info);
        if (ret == false)
        {
            DLOG("向数据库插入数据失败");
            return http_resp(conn, false, websocketpp::http::status_code::bad_request, "用户名已经被占用");
        }
        return http_resp(conn, true, websocketpp::http::status_code::ok, "注册用户成功");
    }
    void login(wsserver_t::connection_ptr &conn)
    {
        //用户登陆功能请求处理
        // 1.获取请求正文&&反序列化，得到用户名和密码
        std::string req = conn->get_request_body();
        // 2.对正文进行json反序列化，得到用户名和密码
        Json::Value login_info;
        Json::Value resp;
        bool ret = json_util::deserialize(req, login_info);
        if (ret == false)
        {
            DLOG("反序列化注册信息失败");
            return http_resp(conn, false, websocketpp::http::status_code::bad_request, "请求正文格式错误");
        }
        // 2.校验完整性，如果验证失败，返回400，如果验证成功
        if (login_info["username"].isNull() || login_info["password"].isNull())
        {
            DLOG("用户名密码不完整");
            return http_resp(conn, false, websocketpp::http::status_code::bad_request, "请输入用户名/密码");
        }
        ret = _ut.login(login_info);
        if (ret == false)
        {
            DLOG("用户名密码错误");
            return http_resp(conn, false, websocketpp::http::status_code::bad_request, "用户名/密码错误");
        }
        // 3.给客户端创建session
        uint64_t uid = login_info["id"].asUInt64();
        session_ptr ssp = _sm.create_session(uid, LOGIN);
        if (ssp.get() == nullptr)
        {
            // 500错误
            DLOG("创建会话失败");
            return http_resp(conn, false, websocketpp::http::status_code::internal_server_error, "创建会话失败");
        }
        //设置session过期时间
        _sm.set_session_expire_time(ssp->ssid(), SESSION_TIMEOUT);
        // 4.设置响应头部：Set-Cookie,讲session id通过Cookie返回
        std::string cookie_ssid = "SSID=" + std::to_string(ssp->ssid());
        conn->append_header("Set-Cookie", cookie_ssid);
        http_resp(conn, true, websocketpp::http::status_code::ok, "登陆成功");
    }
    bool get_cookie_val(const std::string &cookie_str, const std::string &key, std::string &val)
    {
        // Cookie:SSID=xxx; path=/;
        //以; 作为分隔符
        const std::string sep = "; ";
        std::vector<std::string> cookie_arr;
        string_util::split(cookie_str, sep, cookie_arr);
        for (auto str : cookie_arr)
        {
            //对单个Cookie字符串，以=为分割，得到key val
            std::vector<std::string> tmp;
            string_util::split(str, "=", tmp);
            if (tmp.size() != 2)
                continue;
            if (tmp[0] == key)
            {
                val = tmp[1];
                return true;
            }
        }
        return false;
    }
    void info(wsserver_t::connection_ptr &conn)
    {
        //用户信息的获取功能请求处理
        // 1.获取请求信息中的Cookie，从Cookie中获取ssid
        std::string cookie_str = conn->get_request_header("Cookie");
        if (cookie_str.empty())
        {
            //如果没有Cookie信息返回错误信息,让客户端重新登陆
            return http_resp(conn, false, websocketpp::http::status_code::bad_request, "找不到Cookie信息，请重新登陆");
        }
        // 1.1从Cookie中取出ssid
        std::string ssid_str;
        bool ret = get_cookie_val(cookie_str, "SSID", ssid_str);
        if (ret == false)
        {
            //如果Cookie中没有ssid，返回错误;没有ssid信息，让客户端重新登陆
            return http_resp(conn, false, websocketpp::http::status_code::bad_request, "找不到ssid信息,请重新登陆");
        }
        // 2.在session管理中查找对应的会话信息
        session_ptr ssp = _sm.get_session_by_ssid(std::stol(ssid_str));
        if (ssp.get() == nullptr)
        {
            //没有找到session则认为登陆已经过期，需要重新登陆
            return http_resp(conn, false, websocketpp::http::status_code::bad_request, "登陆过期,请重新登陆");
        }
        // 3.从数据库中取出用户信息，进行序列化发送给客户端
        uint64_t uid = ssp->get_user();
        Json::Value user_info;
        ret = _ut.select_by_id(uid, user_info);
        if (ret == false)
        {
            //获取用户信息失败
            return http_resp(conn, false, websocketpp::http::status_code::bad_request, "找不到用户信息,请重新登陆");
        }
        std::string body;
        json_util::serialize(user_info, body);
        DLOG("%s", body.c_str());

        // http_resp(conn,true,websocketpp::http::status_code::ok,"");
        conn->set_body(body);
        conn->append_header("Content-Type", "application/json");
        conn->set_status(websocketpp::http::status_code::ok);
        // 4.刷新session过期时间
        _sm.set_session_expire_time(ssp->ssid(), SESSION_TIMEOUT);
    }
    void http_callback(websocketpp::connection_hdl hdl)
    {
        wsserver_t::connection_ptr conn = _wssrv.get_con_from_hdl(hdl);
        websocketpp::http::parser::request req = conn->get_request();
        std::string method = req.get_method();
        std::string uri = req.get_uri();
        // POST是新增
        if (method == "POST" && uri == "/reg")
        {
            reg(conn);
        }
        else if (method == "POST" && uri == "/login")
        {
            login(conn);
        }
        // GET是查看
        else if (method == "GET" && uri == "/info")
        {
            info(conn);
        }
        else
        {
            file_handler(conn);
        }
    }
    void ws_resp(wsserver_t::connection_ptr &conn, Json::Value &resp)
    {
        std::string body;
        json_util::serialize(resp, body);
        conn->send(body);
    }
    session_ptr get_session_by_cookie(wsserver_t::connection_ptr &conn)
    {
        Json::Value err_resp;
        std::string cookie_str = conn->get_request_header("Cookie");
        if (cookie_str.empty())
        {
            //如果没有Cookie信息返回错误信息,让客户端重新登陆
            err_resp["optype"] = "hall_ready";
            err_resp["reason"] = "找不到Cookie信息，请重新登陆";
            err_resp["result"] = false;
            ws_resp(conn, err_resp);
            return session_ptr();
        }
        // 1.1从Cookie中取出ssid
        std::string ssid_str;
        bool ret = get_cookie_val(cookie_str, "SSID", ssid_str);
        if (ret == false)
        {
            //如果Cookie中没有ssid，返回错误;没有ssid信息，让客户端重新登陆
            err_resp["optype"] = "hall_ready";
            err_resp["reason"] = "找不到ssid信息,请重新登陆";
            err_resp["result"] = false;
            ws_resp(conn, err_resp);
            return session_ptr();
        }
        // 2.在session管理中查找对应的会话信息
        session_ptr ssp = _sm.get_session_by_ssid(std::stol(ssid_str));
        if (ssp.get() == nullptr)
        {
            //没有找到session则认为登陆已经过期，需要重新登陆
            err_resp["optype"] = "hall_ready";
            err_resp["reason"] = "登陆过期,请重新登陆";
            err_resp["result"] = false;
            ws_resp(conn, err_resp);
            return session_ptr();
        }
        //成功
        return ssp;
    }
    void open_game_hall(wsserver_t::connection_ptr &conn)
    {
        //游戏大厅长连接建立成功
        // 1.登陆验证--判断当前客户端是否已经成功登陆
        Json::Value err_resp;
        session_ptr ssp = get_session_by_cookie(conn);
        if (ssp.get() == nullptr)
        {
            return;
        }
        // 2.判断当前客户端是否重复登录
        if (_om.is_in_game_hall(ssp->get_user()) || _om.is_in_game_room(ssp->get_user()))
        {
            err_resp["optype"] = "hall_ready";
            err_resp["reason"] = "玩家重复登录";
            err_resp["result"] = false;
            return ws_resp(conn, err_resp);
        }
        // 3.将当前客户端已经连接加入到游戏大厅
        _om.enter_game_hall(ssp->get_user(), conn);
        // 4.给客户端响应游戏大厅连接建立成功
        Json::Value resp;
        resp["optype"] = "hall_ready";
        resp["result"] = true;
        ws_resp(conn, resp);
        // 5.将session设置为永久
        _sm.set_session_expire_time(ssp->ssid(), SESSION_FOREVER);
    }
    void open_game_room(wsserver_t::connection_ptr &conn)
    {
        // 1.获取当前客户端的session
        Json::Value err_resp;
        session_ptr ssp = get_session_by_cookie(conn);
        if (ssp.get() == nullptr)
        {
            return;
        }
        // 2.当前用户是否已经在用户房间或者游戏大厅中
        if (_om.is_in_game_hall(ssp->get_user()) || _om.is_in_game_room(ssp->get_user()))
        {
            err_resp["optype"] = "room_ready";
            err_resp["reason"] = "玩家重复登录";
            err_resp["result"] = false;
            return ws_resp(conn, err_resp);
        }
        // 3.当前用户是否创建好了房间
        room_ptr rp =_rm.get_room_by_uid(ssp->get_user());
        if (rp.get() == nullptr)
        {
            err_resp["optype"] = "room_ready";
            err_resp["reason"] = "没有找到玩家的房间信息";
            err_resp["result"] = false;
            return ws_resp(conn, err_resp);
        }
        // 4.将当前用户添加到在线用户管理的游戏房间中
        _om.enter_game_room(ssp->get_user(),conn);
        // 5.将session设置为永久存在
        _sm.set_session_expire_time(ssp->ssid(), SESSION_FOREVER);
        // 6.回复房间准备完毕
        Json::Value resp;
        resp["optype"] = "room_ready";
        resp["result"] = true;
        resp["room_id"]=(Json::UInt64)rp->id();
        resp["uid"]=(Json::UInt64)ssp->get_user();
        resp["white_id"]=(Json::UInt64)rp->get_white_user();
        resp["black_id"]=(Json::UInt64)rp->get_black_user();
        ws_resp(conn, resp);
    } 
    void open_callback(websocketpp::connection_hdl hdl)
    {
        //页面关闭了，websocket也就关闭了，依托于页面
        //所以游戏大厅是一个长连接，进入游戏房间又是一个长连接
        // websocket长连接建立成功之后的处理函数
        wsserver_t::connection_ptr conn = _wssrv.get_con_from_hdl(hdl);
        websocketpp::http::parser::request req = conn->get_request();
        std::string uri = req.get_uri();
        if (uri == "/hall")
        {
            open_game_hall(conn);
        }
        else if (uri == "/room")
        {
            open_game_room(conn);
        }
    }

    void close_game_hall(wsserver_t::connection_ptr &conn)
    {
        //游戏大厅长连接断开处理
        Json::Value err_resp;
        session_ptr ssp = get_session_by_cookie(conn);
        if (ssp.get() == nullptr)
        {
            return;
        }
        // 1.将玩家从游戏大厅中移除
        _om.exit_game_hall(ssp->get_user());
        // 2.将session恢复声明周期的管理，设置定时销毁
        _sm.set_session_expire_time(ssp->ssid(), SESSION_TIMEOUT);
    }
    void close_game_room(wsserver_t::connection_ptr &conn)
    {
        DLOG("close_game_room");
        //获取会话信息，识别客户端
        Json::Value err_resp;
        session_ptr ssp = get_session_by_cookie(conn);
        if (ssp.get() == nullptr)
        {
            return;
        }
        //1.将玩家从在线用户管理中移除(从房间移除)
        _om.exit_game_room(ssp->get_user());
        //2.将seesion恢复生命周期的管理，设置定时销毁
        _sm.set_session_expire_time(ssp->ssid(), SESSION_TIMEOUT);
        //3.将玩家从游戏房间中移除，房间中所有用户退出就会销毁房间
        _rm.remove_room_user(ssp->get_user());
    }
    void close_callback(websocketpp::connection_hdl hdl)
    {
        // websocket连接断开前的处理
        //判断是哪一个长连接
        wsserver_t::connection_ptr conn = _wssrv.get_con_from_hdl(hdl);
        websocketpp::http::parser::request req = conn->get_request();
        std::string uri = req.get_uri();
        if (uri == "/hall")
        {
            close_game_hall(conn);
        }
        else if (uri == "/room")
        {
            close_game_room(conn);
        }
    }
    void msg_game_hall(wsserver_t::connection_ptr &conn, wsserver_t::message_ptr msg)
    {
        Json::Value resp;
        // 1.身份验证，当前客户端是哪个玩家
        Json::Value err_resp;
        std::string resp_body;
        session_ptr ssp = get_session_by_cookie(conn);
        if (ssp.get() == nullptr)
        {
            return;
        }
        // 2.获取请求信息
        Json::Value req;
        std::string req_body = msg->get_payload();
        bool ret = json_util::deserialize(req_body, req);
        if (ret == false)
        {
            resp["result"] = false;
            resp["reason"] = "请求信息解析失败";
            return ws_resp(conn, resp);
        }
        // 3.对于请求进行处理:开始对战匹配或者停止对战匹配
        if (!req["optype"].isNull() && req["optype"].asString() == "match_start")
        {
            //开始对战匹配：通过匹配模块，将用户添加到匹配队列中
            _mm.add(ssp->get_user());
            resp["optype"] = "match_start";
            resp["result"] = true;
            return ws_resp(conn, resp);
        }
        else if (!req["optype"].isNull() && req["optype"].asString() == "match_stop")
        {
            //停止对战匹配：通过匹配模块，将用户从匹配队列移除
            _mm.del(ssp->get_user());
            resp["optype"] = "match_stop";
            resp["result"] = true;
            return ws_resp(conn, resp);
        }
        else
        {
            resp["optype"] = "unknow";
            resp["result"] = false;
            return ws_resp(conn, resp);
        }
        // 4.给客户端响应
    }
    void msg_game_room(wsserver_t::connection_ptr &conn, wsserver_t::message_ptr msg)
    {
        //1.获取客户端session，识别客户端身份
        session_ptr ssp = get_session_by_cookie(conn);
        if (ssp.get() == nullptr)
        {
            return;
        }
        //2.获取客户端房间信息
        Json::Value err_resp;
        room_ptr rp =_rm.get_room_by_uid(ssp->get_user());
        if (rp.get() == nullptr)
        {
            err_resp["optype"] = "unknow";
            err_resp["reason"] = "没有找到玩家的房间信息";
            err_resp["result"] = false;
            return ws_resp(conn, err_resp);
        }
        //3.对消息进行反序列化
        Json::Value req;
        std::string req_body=msg->get_payload();
        bool ret=json_util::deserialize(req_body,req);
        if(ret==false)
        {
            err_resp["optype"] = "unknow";
            err_resp["reason"] = "请求解析失败";
            err_resp["result"] = false;
            return ws_resp(conn, err_resp);
        }
        //4.通过房间模块进行消息的处理
        rp->handle_request(req);
    }
    void message_callback(websocketpp::connection_hdl hdl, wsserver_t::message_ptr msg)
    {
        wsserver_t::connection_ptr conn = _wssrv.get_con_from_hdl(hdl);
        websocketpp::http::parser::request req = conn->get_request();
        std::string uri = req.get_uri();
        if (uri == "/hall")
        {
            msg_game_hall(conn, msg);
        }
        else if (uri == "/room")
        {
            msg_game_room(conn,msg);
        }
    }

public:
    gobang_server(const char *host, const char *user, const char *password, const char *db, const int port,
                  const std::string &wwwroot = WWWROOT)
        : _web_root(wwwroot), _ut(host, user, password, db, port), _om(), _rm(&_ut, &_om), _sm(&_wssrv), _mm(&_rm, &_ut, &_om)
    {
        // 2.设置打印日志
        _wssrv.set_access_channels(websocketpp::log::alevel::none); //禁止打印
        // 3.初始化asio调度器
        _wssrv.init_asio();
        //设置地址重用
        _wssrv.set_reuse_addr(true);
        // 4.设置回调函数
        // 4.1 请求回调处理函数
        _wssrv.set_http_handler(bind(&gobang_server::http_callback, this, std::placeholders::_1));
        // 4.2 握手成功回调处理函数
        _wssrv.set_open_handler(bind(&gobang_server::open_callback, this, std::placeholders::_1));
        // 4.3 连接关闭回调处理函数
        _wssrv.set_close_handler(bind(&gobang_server::close_callback, this, std::placeholders::_1));
        // 4.4 消息回调处理函数
        _wssrv.set_message_handler(bind(&gobang_server::message_callback, this, std::placeholders::_1, std::placeholders::_2));
    }
    //启动服务器
    void start(int port)
    {
        // 5.设置监听窗口
        _wssrv.listen(port);
        // 6.开始获取新连接
        _wssrv.start_accept();
        // 7.启动服务器
        _wssrv.run();
    }
};
#endif