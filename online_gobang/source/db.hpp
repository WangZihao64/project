#ifndef DB_H
#define DB_H
#include "util.hpp"
#include <mutex>
#include <cassert>
class user_table
{
public:
    user_table(const char* host, const char *user, const char *password, const char *db, const int port)
    {
        _mysql = mysql_util::mysql_create(host, user, password, db, port);
        assert(_mysql != NULL);
    }
    bool insert(Json::Value &user) // 注册时新增用户
    {
#define INSERT_USER "insert into users values (null,'%s',password('%s'),1000,0,0);"
        if(user["password"].isNull()||user["username"].isNull())
        {
            DLOG("please input password or name");
            return false;
        }
        char buffer[4096]={0};
        Json::Value root;
        sprintf(buffer, INSERT_USER, user["username"].asCString(), user["password"].asCString());
        bool ret = mysql_util::mysql_exec(_mysql, buffer);

        if (ret == false)
        {
            DLOG("insert user failed");
            return false;
        }
        return true;
    }
    bool login(Json::Value &user) // 登陆验证&&返回详细用户信息
    {
#define LOGIN_USER "select id,score,total_count,win_count from users where username='%s' and password=password('%s');"
        char buffer[1024];
        if(user["password"].isNull()||user["username"].isNull())
        {
            DLOG("please input password or name");
            return false;
        }
        sprintf(buffer, LOGIN_USER, user["username"].asCString(), user["password"].asCString());
        MYSQL_RES *res = NULL;
        {
            //查询和保存结果虽然单独看都是原子的，但是放到一起就不是了，需要加锁
            //查询过后，如果其他线程执行了别的操作，就会导致保存结果的sql语句出现问题
            std::unique_lock<std::mutex> lock(_mutex);
            bool ret = mysql_util::mysql_exec(_mysql, buffer);
            if (ret == false)
            {
                DLOG("user %s login failed", user["username"].asCString());
                return false;
            }
            res = mysql_store_result(_mysql);
            if (res == NULL)
            {
                DLOG("have no login user info!");
                return false;
            }
        }
        int row_num = mysql_num_rows(res);

        if (row_num != 1)
        {
            DLOG("the user information not unique!");
            return false;
        }
        MYSQL_ROW row = mysql_fetch_row(res);
        user["id"] = (Json::UInt64)std::stol(row[0]);
        user["score"] = (Json::UInt64)std::stol(row[1]);
        user["total_count"] = std::stoi(row[2]);
        user["win_count"] = std::stoi(row[3]);
        mysql_free_result(res);
        return true;
    }
    bool select_by_name(const std::string &name, Json::Value &user) // 用过用户名获取用户信息
    {
#define USER_BY_NAME "select id,score,total_count,win_count from users where username='%s';"
        char buffer[1024];
        sprintf(buffer, USER_BY_NAME, name.c_str());
        MYSQL_RES *res = NULL;
        {
            std::unique_lock<std::mutex> lock(_mutex);
            bool ret = mysql_util::mysql_exec(_mysql, buffer);
            if (ret == false)
            {
                DLOG("get user by name failed!");
                return false;
            }
            res = mysql_store_result(_mysql);
            if (res == NULL)
            {
                DLOG("have no user info!");
                return false;
            }
        }
        int row_num = mysql_num_rows(res);
        std::cout << row_num << std::endl;
        if (row_num != 1)
        {
            DLOG("the user information queried is not unique!");
            return false;
        }
        MYSQL_ROW row = mysql_fetch_row(res);
        user["id"] = (Json::UInt64)std::stol(row[0]);
        user["username"] = name;
        user["score"] = (Json::UInt64)std::stol(row[1]);
        user["total_count"] = std::stoi(row[2]);
        user["win_count"] = std::stoi(row[3]);
        mysql_free_result(res);
        return true;
    }
    bool select_by_id(const uint64_t id, Json::Value &user) // 用过id获取用户信息
    {
#define USER_BY_ID "select username,score,total_count,win_count from users where id=%d;"
        char buffer[1024];
        sprintf(buffer, USER_BY_ID, id);
        MYSQL_RES *res = NULL;
        {
            std::unique_lock<std::mutex> lock(_mutex);
            bool ret = mysql_util::mysql_exec(_mysql, buffer);
            if (ret == false)
            {
                DLOG("get user by id failed");
                return false;
            }
            res = mysql_store_result(_mysql);
            if (res == NULL)
            {
                DLOG("have no user info!");
                return false;
            }
        }
        int row_num = mysql_num_rows(res);
        if (row_num != 1)
        {
            DLOG("the user information queried is not unique!");
            return false;
        }
        MYSQL_ROW row = mysql_fetch_row(res);
        user["id"] = (Json::UInt64)id;
        user["username"] = row[0];
        user["score"] = (Json::UInt64)std::stol(row[1]);
        user["total_count"] = std::stoi(row[2]);
        user["win_count"] = std::stoi(row[3]);
        mysql_free_result(res);
        return true;
    }
    bool win(uint64_t id) // 胜利时，天梯分增加，场次增加，胜利增加
    {
// 胜利时，天梯分数每次增加30分，失败就减少30分
#define USER_WIN "update users set score=score+30,win_count=win_count+1,total_count=total_count+1 where id=%d;"
        char buffer[1024];
        sprintf(buffer, USER_WIN, id);
        bool ret = mysql_util::mysql_exec(_mysql, buffer);
        if (ret == false)
        {
            DLOG("update win user info failed");
            return false;
        }
        return true;
    }
    bool lose(uint64_t id) // 失败时，天梯分下降，场次增加，胜利不变
    {
// 胜利时，天梯分数每次增加30分，失败就减少30分
#define USER_LOSE "update users set score=score-30,total_count=total_count+1 where id=%d;"
        char buffer[1024];
        sprintf(buffer, USER_LOSE, id);
        bool ret = mysql_util::mysql_exec(_mysql, buffer);
        if (ret == false)
        {
            DLOG("update lose user info failed");
            return false;
        }
        return true;
    }
    ~user_table()
    {
        mysql_util::mysql_destroy(_mysql);
        _mysql = NULL;
    }

private:
    MYSQL *_mysql;
    std::mutex _mutex;
};
#endif