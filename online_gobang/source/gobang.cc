#include"logger.hpp"
#include"util.hpp"
#include"db.hpp"
#include"online.hpp"
#include"room.hpp"
#include"matcher.hpp"
#include"server.hpp"
const char* host="127.0.0.1";
const char* user="root";
const char* password=NULL;
const char* db="gobang";
const int port=3306;
const char* sep=",";
void testlog()
{
    // DLOG("hello world");
    // ILOG("%s %d","王子豪",18);
    // DLOG("%s %d","曹之颖",18);
    // ELOG("%s %d","袁浩男",18);
}
// void testsql()
// {
//     MYSQL* mysql=mysql_util::mysql_create(host,user,password,db,port);
//     char* sql="insert into users values (null,'wae','21313123aaw',1,1,1);";
//     mysql_util::mysql_exec(mysql,sql);
// }
void testjson()
{
    Json::Value root;
    root["姓名"]="王子豪";
    root["年龄"]=18;
    root["成绩"].append(90);
    root["成绩"].append(80);
    root["成绩"].append(95);
    std::string str;
    json_util::serialize(root,str);
    DLOG("%s",str.c_str());
    json_util::deserialize(str,root);
    std::cout << "姓名" << root["姓名"].asString() << std::endl;
    std::cout << "年龄" << root["年龄"].asInt() << std::endl;
    int sz=root["成绩"].size();
    for(int i=0;i<sz;++i)
    {
        std::cout << root["成绩"][i].asFloat() << " ";
    }
    std::cout << std::endl;
}
void teststring()
{
    std::string str="wangzihao,caozhiying,,lutianyi";
    std::vector<std::string> v;
    string_util::split(str,sep,v);
    for(int i=0;i<v.size();++i)
    {
        DLOG("%s ",v[i].c_str());
    }
}
void testfile()
{
    std::string filename="test.txt";
    std::string body;
    file_util::read(filename,body);
    DLOG("%s",body.c_str());
}
void testdb()
{
    user_table ut(host,user,password,db,port);
    Json::Value user;
    user["username"] = "wangletian";
    user["password"] = "2131aaw";
    // bool ret = ut.insert(user);
    // ut.lose(1);
    // std::string str;
    // json_util::serialize(user,str);
    // std::cout << str << std::endl;
    bool ret=ut.login(user);
    if(ret==false)
    {
        ELOG("false");
    }
}
void testonline()
{
    online_manager om;
    wsserver_t::connection_ptr conn;
    om.enter_game_room(2,conn);
    bool ret=om.is_in_game_room(2);
    if(ret==true)
    {
        DLOG("in hall");
    }
    om.exit_game_room(2);
    ret=om.is_in_game_room(2);
    if(ret==true)
    {
        DLOG("in hall");
    }
}
void testroom()
{
    user_table ut(host,user,password,db,port);
    online_manager om;
    room r(10,&ut,&om);
}
void testroommanager()
{
    user_table ut(host,user,password,db,port);
    online_manager om;
    room_manager rm(&ut,&om);
    room_ptr pt=rm.create_room(10,20);
}
void testmatcher()
{
    user_table ut(host,user,password,db,port);
    online_manager om;
    room_manager rm(&ut,&om);
    matcher mc(&rm,&ut,&om);
}
void testserver()
{
    gobang_server gs(host,user,password,db,port);
    gs.start(8081);
}
int main()
{
    // testlog();
    // testsql();
    // testjson();
    // teststring();
    // testfile();
    // testdb();
    // testonline();
    // testroom();
    // testroommanager();
    // testmatcher();
    testserver();
    return 0;
}