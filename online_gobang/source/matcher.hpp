#ifndef MATCHER_H
#define MATCHER_H
#include<list>
#include"room.hpp"
#include"online.hpp"
#include"util.hpp"
#include<condition_variable>
template<class T>
class matcher_queue
{
    private:
    //用链表而不直接使用queue的原因是我们中间有删除数据的需要
    std::list<T> _list;
    std::mutex _mutex;
    //这个条件变量主要是为了阻塞消费者，后边使用的时候：队列中元素个数<2则阻塞
    std::condition_variable _cond;
    public:
    matcher_queue()
    {
    }
    ~matcher_queue()
    {
    }
    //入队数据，并唤醒数据
    void push(const T& data)
    {
        std::unique_lock<std::mutex> lock(_mutex);
        _list.push_back(data);
        _cond.notify_all();
    }
    //出队数据
    bool pop(T& data)
    {
        std::unique_lock<std::mutex> lock(_mutex);
        if(_list.empty())
        {
            return false;
        }
        data=_list.front();
        _list.pop_front();
        return true;
    }
    //移除指定数据
    bool remove(T& data)
    {
        std::unique_lock<std::mutex> lock(_mutex);
        _list.remove(data);
    }
    //获取元素个数
    int size()
    {
        return _list.size();
    }
    //获取队列是否为空
    bool empty()
    {
        std::unique_lock<std::mutex> lock(_mutex);
        return _list.empty();
    }
    //阻塞线程
    void wait()
    {
        std::unique_lock<std::mutex> lock(_mutex);
        _cond.wait(lock);
    }
};
class matcher
{
    private:
    //普通、高手、大神匹配队列
    matcher_queue<uint64_t> _q_normal;
    matcher_queue<uint64_t> _q_master;
    matcher_queue<uint64_t> _q_super;
    //对应三个匹配队列的处理线程
    std::thread _th_normal;
    std::thread _th_master;
    std::thread _th_super;
    room_manager* _rm;
    user_table* _ut;
    online_manager* _om;

    //线程的入口函数
    void th_normal_entry()
    {
        return handle_match(_q_normal);
    }
    void th_master_entry()
    {
        return handle_match(_q_master);
    }
    void th_super_entry()
    {
        return handle_match(_q_super);
    }
    void handle_match(matcher_queue<uint64_t>& queue)
    {
        while(1)
        {
            //1.判断队列人数是否大于2，小于2阻塞等待
            while(queue.size()<2)
            {
                queue.wait();
            }
            //2.人数够了，出队两个玩家
            uint64_t uid1,uid2;
            bool ret=queue.pop(uid1);
            if(ret==false)
            {
                continue;;
            }
            ret=queue.pop(uid2);
            if(ret==false)
            {
                add(uid1);
                continue;
            }
            //3.校验两个玩家是否在线，如果有人掉线，则把另一个人添加到队列
            wsserver_t::connection_ptr conn1=_om->get_conn_from_hall(uid1);
            if(conn1.get()==nullptr)
            {
                add(uid2);
                continue;
            }
            wsserver_t::connection_ptr conn2=_om->get_conn_from_hall(uid2);
            if(conn2.get()==nullptr)
            {
                add(uid1);
                continue;
            }
            //4.为两个玩家创建房间，并把两个玩家加入房间
            room_ptr pt=_rm->create_room(uid1,uid2);
            if(pt.get()==nullptr)
            {
                add(uid1);
                add(uid2);
                continue;
            }
            //5.对两个玩家进行响应
            Json::Value resp;
            resp["optype"]="match_success";
            resp["result"]="true";
            std::string body;
            json_util::serialize(resp,body);
            conn1->send(body);
            conn2->send(body);
        }
    }
    public:
    matcher(room_manager* rm,user_table* ut,online_manager* om)
    :_rm(rm)
    ,_ut(ut)
    ,_om(om)
    ,_th_normal(std::thread(&matcher::th_normal_entry,this))
    ,_th_master(std::thread(&matcher::th_master_entry,this))
    ,_th_super(std::thread(&matcher::th_super_entry,this))
    {
        DLOG("游戏匹配模块初始化完毕");
    }
    bool add(uint64_t uid)
    {
        //根据玩家分数，来判定玩家档次，添加到不同的匹配队列
        //1.根据用户id，获取玩家信息
        Json::Value user;
        bool ret=_ut->select_by_id(uid,user);
        if(ret==false)
        {
            DLOG("获取玩家信息失败,uid:%d",uid);
            return false;
        }
        int score=user["score"].asInt();
        //2.添加到指定队列
        if(score<2000)
        {
            _q_normal.push(uid);
        }
        else if(score>=200&&score<3000)
        {
            _q_master.push(uid);
        }
        else
        {
            _q_super.push(uid);
        }
        return true;
    }
    bool del(uint64_t uid)
    {
        Json::Value user;
        bool ret=_ut->select_by_id(uid,user);
        if(ret==false)
        {
            DLOG("获取玩家信息失败,uid:%d",uid);
            return false;
        }
        int score=user["score"].asInt();
        //2.添加到指定队列
        if(score<2000)
        {
            _q_normal.remove(uid);
        }
        else if(score>=200&&score<3000)
        {
            _q_master.remove(uid);
        }
        else
        {
            _q_super.remove(uid);
        }
        return true;
    }
};
#endif