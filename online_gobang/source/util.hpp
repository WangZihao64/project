#ifndef UTIL_H
#define UTIL_H
#include "logger.hpp"
#include <mysql/mysql.h>
#include <stdio.h>
#include <jsoncpp/json/json.h>
#include <sstream>
#include <memory>
#include <vector>
#include <fstream>
#include<websocketpp/server.hpp>
//非ssl/tls加密的一个asio框架
#include<websocketpp/config/asio_no_tls.hpp>
typedef websocketpp::server<websocketpp::config::asio> wsserver_t;
class mysql_util
{
public:
    static MYSQL *mysql_create(const char *HOST, const char *USER, const char *PASSWORD, const char *DB, const int PORT)
    {
        MYSQL *mysql = mysql_init(NULL);
        if (mysql == NULL)
        {
            ELOG("mysql init failed");
            return nullptr;
        }
        if (mysql_real_connect(mysql, HOST, USER, PASSWORD, DB, PORT, NULL, 0) == NULL)
        {
            ELOG("mysql_real_connect failed: %s", mysql_error(mysql));
            // 记得关闭
            mysql_close(mysql);
            return nullptr;
        }
        if (mysql_set_character_set(mysql, "utf8") != 0)
        {
            ELOG("set client character failed : %s", mysql_error(mysql));
            mysql_close(mysql);
            return nullptr;
        }
        return mysql;
    }
    static bool mysql_exec(MYSQL *mysql, std::string sql)
    {
        int ret=mysql_query(mysql, sql.c_str());
        if (ret != 0)
        {
            ELOG("mysql_query failed: %s", mysql_error(mysql));
            return false;
        }
        return true;
    }
    static void mysql_destroy(MYSQL *mysql)
    {
        if (mysql != NULL)
        {
            mysql_close(mysql);
        }
    }
};
class json_util
{
public:
    static bool serialize(const Json::Value &root, std::string &str)
    {
        // 实例化一个StreamWriterBuilder工厂类对象
        Json::StreamWriterBuilder swb;
        // 通过StreamWriterBuilder工厂类对象生产一个StreamWriter对象
        std::unique_ptr<Json::StreamWriter> sw(swb.newStreamWriter());
        // 使用StreamWriter对象，对Value对象中存储的数据进行序列化
        std::stringstream ss;
        int ret = sw->write(root, &ss);
        if (ret != 0)
        {
            ELOG("json serialize failed");
            return false;
        }
        str=ss.str();
        return true;
    }
    static bool deserialize(const std::string str, Json::Value &root)
    {
        // 实例化一个CharReaderBuilder工厂类对象
        Json::CharReaderBuilder crb;
        // 通过CharReaderBuilder工厂类对象生产一个CharReader对象
        std::unique_ptr<Json::CharReader> cr(crb.newCharReader());
        std::string err;
        // 使用CharReader对象进行json字符串格式str的反序列化
        bool ret=cr->parse(str.c_str(), str.c_str() + str.size(), &root, &err);
        if(ret==false)
        {
            ELOG("json deserialize failed");
            return false;
        }
        return true;
    }
};
class string_util
{
    public:
    static int split(const std::string& src,const std::string& sep,std::vector<std::string>& des)
    {
        size_t start=0;
        size_t pos=0;
        while(start<src.size())
        {
            pos=src.find(sep,start);
            if(pos==std::string::npos)
            {
                des.push_back(src.substr(start));
                break;
            }
            if(start!=pos)
            {
                des.push_back(src.substr(start,pos-start));
            }
            start=pos+sep.size();
        }
        return des.size();
    }
};
class file_util {
   public:
        static bool read(const std::string &filename, std::string &body) {
            //打开文件
            std::ifstream ifs(filename.c_str(), std::ios::binary);
            if (ifs.is_open() == false) {
                ELOG("%s file open failed!!", filename.c_str());
                return false;
            }
            //获取文件大小
            size_t fsize = 0;
            ifs.seekg(0, std::ios::end);
            fsize = ifs.tellg();
            ifs.seekg(0, std::ios::beg);
            body.resize(fsize);
            //将文件所有数据读取出来
            ifs.read(&body[0], fsize);
            if (ifs.good() == false) {
                ELOG("read %s file content failed!", filename.c_str());
                ifs.close();
                return false;
            }
            //关闭文件
            ifs.close();
            return true;
        }
};
#endif