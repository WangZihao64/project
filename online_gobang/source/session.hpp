#ifndef SESSION_H
#define SESSION_H
#include <iostream>
#include <unordered_map>
#include "util.hpp"
typedef enum
{
    UNLOGIN,
    LOGIN
} ss_state;
class session
{
private:
    uint64_t _ssid;            //标识符
    uint64_t _uid;             // session对应的用户ID
    ss_state _state;           //用户的登陆状态
    wsserver_t::timer_ptr _tp; // session关联的定时器
public:
    session(uint64_t ssid)
        : _ssid(ssid)
    {
        DLOG("session %p 被创建", this);
    }
    ~session()
    {
        DLOG("session %p 被释放", this);
    }
    uint64_t ssid()
    {
        return _ssid;
    }
    void set_user(uint64_t uid)
    {
        _uid = uid;
    }
    uint64_t get_user()
    {
        return _uid;
    }
    bool is_login()
    {
        return _state == LOGIN;
    }
    void set_timer(const wsserver_t::timer_ptr &tp)
    {
        _tp = tp;
    }
    wsserver_t::timer_ptr &get_timer()
    {
        return _tp;
    }
    void set_state(ss_state state)
    {
        _state = state;
    }
};
#define SESSION_FOREVER -1
#define SESSION_TIMEOUT 30000
using session_ptr = std::shared_ptr<session>;
class session_manager
{
private:
    uint64_t _next_ssid;
    std::mutex _mutex;
    std::unordered_map<uint64_t, session_ptr> _session;
    wsserver_t *_server;

public:
    session_manager(wsserver_t *server)
        : _next_ssid(1), _server(server)
    {
        DLOG("session管理器初始化完毕");
    }
    ~session_manager()
    {
        DLOG("session管理器即将销毁");
    }
    //只有登陆后才创建session
    //有些网站是登陆前也会创建session，进行别的操作需要登陆，然后又会创建新的session
    //创建session
    session_ptr create_session(uint64_t uid, ss_state state)
    {
        std::unique_lock<std::mutex> lock(_mutex);
        session_ptr ssp(new session(_next_ssid));
        ssp->set_state(state);
        ssp->set_user(uid);
        _session.insert(std::make_pair(_next_ssid, ssp));
        ++_next_ssid;
        return ssp;
    }
    //获取session
    session_ptr get_session_by_ssid(uint64_t ssid)
    {
        std::unique_lock<std::mutex> lock(_mutex);
        auto it = _session.find(ssid);
        if (it == _session.end())
        {
            return session_ptr();
        }
        return it->second;
    }
    void append_session(session_ptr &ssp)
    {
        _session.insert(std::make_pair(ssp->ssid(), ssp));
    }
    //对会话设置过期时间
    void set_session_expire_time(uint64_t ssid, int ms)
    {
        //依赖于websocketpp的定时器来完成session生命周期的管理
        //在http通信的时候（登陆、注册） session应该具备生命周期，指定时间无通信后删除
        //在建立websocket长连接之后，session应该永久存在
        //登陆之后，创建session，session需要在指定时间无通信删除
        //但是进入游戏大厅或者游戏房间，这个session永久存在
        //等待退游戏大厅或者游戏房间，这个session应该被重新设置为临时的具有生命周期，在长时间无通信删除
        session_ptr ssp = get_session_by_ssid(ssid);
        if (ssp.get() == nullptr)
        {
            return;
        }
        wsserver_t::timer_ptr tp = ssp->get_timer();
        // 1.在session永久存在的情况下,设置永久存在
        if (tp.get() == nullptr && ms == SESSION_FOREVER)
        {
            return;
        }
        // 2.在session永久存在的情况下，设置指定时间后被删除的任务
        else if (tp.get() == nullptr && ms != SESSION_FOREVER)
        {
            wsserver_t::timer_ptr tmp_pt = _server->set_timer(ms, std::bind(&session_manager::remove_session, this, ssid));
            ssp->set_timer(tmp_pt);
        }
        // 3.在session设置了定时删除的情况下，将session设置永久存在
        else if (tp.get() != nullptr && ms == SESSION_FOREVER)
        {
            //删除定时任务会导致任务直接被执行
            tp->cancel();
            ssp->set_timer(wsserver_t::timer_ptr());
            //因为这个取消定时任务不是立即取消的，因此重新给session管理器中，添加一个session信息，且添加的时候使用定时器，不是立即添加
            wsserver_t::timer_ptr tmp_pt=_server->set_timer(0,std::bind(&session_manager::append_session,this,ssp));
        }
        // 4.在session设置了定时删除的情况下，将session重制删除时间
        else if(tp.get() != nullptr && ms!= SESSION_FOREVER)
        {
            tp->cancel();
            wsserver_t::timer_ptr tmp=_server->set_timer(0,std::bind(&session_manager::append_session,this,ssp));
            //重新设置session关联的定时器
            ssp->set_timer(wsserver_t::timer_ptr());
            wsserver_t::timer_ptr tmp_pt = _server->set_timer(ms, std::bind(&session_manager::remove_session, this, ssid));
            ssp->set_timer(tmp_pt);
        }
    }
    //销毁session
    void remove_session(int64_t ssid)
    {
        std::unique_lock<std::mutex> lock(_mutex);
        _session.erase(ssid);
    }
};
#endif