#include<stdio.h>
#include<mysql/mysql.h>
#define HOST "127.0.0.1"
#define USER "root"
#define PASSWORD "NULL"
#define DB "stu"
#define PORT 3306

int main()
{
    //1.初始化mysql句柄
    //2.连接mysql服务器
    //3.设置当前客户端的字符集
    //4.选择操作的数据库
    //5.执行sql语句
    //6.保存查询结果到本地
    //7.获取结果集中的⾏数和列数
    //8.遍历结果集, 并且这个接⼝会保存当前读取结果位置，每次获取的都是下⼀条数据
    //9.释放结果集
    //10.关闭数据库客⼾端连接，销毁句柄
    //如果传NULL，底层会给我们malloc一个空间 然后返回
    MYSQL* mysql=mysql_init(NULL);
    if(mysql==NULL)
    {
        printf("mysql init failed\n");
        return -1;
    }
    if(mysql_real_connect(mysql,HOST,USER,PASSWORD,DB,PORT,NULL,0)==NULL)
    {
        printf("mysql_real_connect failed: %s\n",mysql_errno(mysql));
        //记得关闭
        mysql_close(mysql);
        return -1;
    }
    if (mysql_set_character_set(mysql, "utf8") != 0) {
        printf("set client character failed : %s\n", mysql_error(mysql));
        mysql_close(mysql);
        return -1;
    }
    // char* sql="insert into stu values ('曹之颖',23);";
    char* sql="select * from stu";
    if(mysql_query(mysql,sql)!=0)
    {
        printf("mysql_query failed: %s\n",mysql_errno(mysql));
        //记得关闭
        mysql_close(mysql);
        return -1;
    }
    MYSQL_RES* res=mysql_store_result(mysql);
    if(res==NULL)
    {
        printf("mysql_store_result failed: %s\n",mysql_errno(mysql));
        mysql_close(mysql);
        return -1;
    }
    int num_row=mysql_num_rows(res);
    int num_col=mysql_num_fields(res);
    for(int i=0;i<num_row;++i)
    {
        MYSQL_ROW row=mysql_fetch_row(res);
        for(int j=0;j<num_col;++j)
        {
            printf("%s\t",row[j]);
        }
        printf("\n");
    }
    mysql_free_result(res);
    mysql_close(mysql);
    return 0;
}