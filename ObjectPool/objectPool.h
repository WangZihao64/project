//
// Created by 王子豪 on 2023/5/18.
//

#ifndef OBJECTPOOL_OBJECTPOOL_H
#define OBJECTPOOL_OBJECTPOOL_H

#endif //OBJECTPOOL_OBJECTPOOL_H

#include<iostream>

template<class T>
class objectPool {
public:
    objectPool()
    :_memory(nullptr)
    ,_freelist(nullptr)
    ,_leftsize(0)
    {}
    T* New()
    {
        T* obj= nullptr;
        //还回来的内存有剩余的空间，优先使用
        if(_freelist)
        {
            void* next=*((void**)_freelist);
            obj=(T*)_freelist;
            _freelist=next;
        }
        else
        {
            //如果剩余空间不够使用才需要开辟空间
            if(_leftsize<sizeof(T))
            {
                _leftsize=128*1024;
                _memory=(char*)malloc(_leftsize);
                if(_memory== nullptr)
                {
                    throw std::bad_alloc();
                }
            }
            obj=(T*)_memory;
            //如果要的空间仅仅只是char类型，那么必须减去一个指针，原因在于我们还要存储下一个位置
            //例如最后空间大小是1个字节，无法存储下一个地址
            int objsz=sizeof(T)<sizeof(void*)?sizeof(void*):sizeof(T);
            _leftsize-=objsz;
            _memory+=objsz;
        }
        //显示调用T的构造函数
        new(obj)T;
        return obj;
    }
    void Del(T* obj)
    {
        //显示调用T的析构函数
        obj->~T();
        //把申请到的内存放到freelist里面
        *(void**)obj=_freelist;
        _freelist=obj;
    }
private:
    //需要开辟的内存
    char *_memory;
    //指向释放的内存
    void *_freelist;
    int _leftsize;
};
