//
// Created by 王子豪 on 2023/5/18.
//
#include"objectPool.h"
struct TreeNode
{
    int _val;
    TreeNode* _left;
    TreeNode* _right;

    TreeNode()
            :_val(0)
            , _left(nullptr)
            , _right(nullptr)
    {}
};
int main()
{
    // 申请释放的轮次
    const size_t Rounds = 2;

    // 每轮申请释放多少次
    const size_t N = 2;

    std::vector<TreeNode*> v1;
    v1.reserve(N);

    size_t begin1 = clock();
    for (size_t j = 0; j < Rounds; ++j)
    {
        for (int i = 0; i < N; ++i)
        {
            v1.push_back(new TreeNode);
        }
        for (int i = 0; i < N; ++i)
        {
            delete v1[i];
        }
        v1.clear();
    }

    size_t end1 = clock();

    std::vector<TreeNode*> v2;
    v2.reserve(N);

    objectPool<TreeNode> TNPool;
    size_t begin2 = clock();
    for (size_t j = 0; j < Rounds; ++j)
    {
        for (int i = 0; i < N; ++i)
        {
            v2.push_back(TNPool.New());
        }
        for (int i = 0; i < N; ++i)
        {
            TNPool.Del(v2[i]);
        }
        v2.clear();
    }
    size_t end2 = clock();

    std::cout << "new cost time:" << end1 - begin1 << std::endl;
    std::cout << "object pool cost time:" << end2 - begin2 << std::endl;
    return 0;
}