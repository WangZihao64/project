//
// Created by 王子豪 on 2023/5/19.
//

#ifndef TCMALLOC_THREADCACHE_H
#define TCMALLOC_THREADCACHE_H

#endif //TCMALLOC_THREADCACHE_H
#pragma once
#include "Common.h"
class ThreadCache
{
public:
    //申请和释放内存对象
    void* Allocate(size_t size);
    void Deallocate(void* ptr,size_t size);
    //从中心缓存获取对象
    void* FetchFromCentralCache(size_t index,size_t bytes);
    // 释放对象时，链表过长时，回收内存回到中心缓存
    void ListTooLong(FreeList& list, size_t size);

private:
    FreeList _freelist[NFREELIST];
};
//windows下加__declspec(thread)这个tls_threadcache就是每个线程独有的全局的指针了，Linux有自己的用法
#ifdef _WIN32
static __declspec(thread) ThreadCache* pTLSThreadCache = nullptr;
#else
static __thread ThreadCache* pTlsThreadCache= nullptr;
#endif
