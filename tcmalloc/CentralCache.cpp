//
// Created by 王子豪 on 2023/5/24.
//

#include <assert.h>
#include "CentralCache.h"
#include "PageCache.h"
//从page cache获得内存
Span* CentralCache::GetOneSpan(SpanList &list, size_t byte_size)
{
    Span* it=list.Begin();
    while(it!=list.End())
    {
        if(it->_freeList!= nullptr)
        {
            return it;
        }
        else
        {
            it = it->_next;
        }
    }
    //先把central cache的桶锁解除，这样其他线程释放没存不会阻塞
    list._mtx.unlock();
    PageCache::GetInstance()->_pageMtx.lock();
    Span* span=PageCache::GetInstance()->NewSpan(SizeClass::NumMovePage(byte_size));
    span->_objSize=byte_size;
    span->_isUse=true;
    PageCache::GetInstance()->_pageMtx.unlock();
    //对span进行切分不需要加锁，他没有被挂到list上，其他线程看不到
    //计算span的大块内存的起始地址和大块内存的大小
    //例如第100页的位置是 100*8*1024
    char* start=(char*)(span->_pageId<<PAGE_SHIFT);
    size_t bytes=span->_n<<PAGE_SHIFT;
    char* end=start+bytes;

    //把大块内存切成自由链表链接起来
    span->_freeList=start;
    start+=byte_size;
    void* tail=span->_freeList;
    //计数看看切的对不对
    int i=1;
    while(start<end)
    {
        ++i;
        NextObj(tail)=start;
        tail= NextObj(tail);
        start+=byte_size;
    }
    NextObj(tail)= nullptr;
    //切好span以后，被挂到桶上的时候，再加锁
    list._mtx.lock();
    list.PushFront(span);
    return span;
}
size_t CentralCache::FetchRangeObj(void *&start, void *&end, size_t batchNum, size_t size)
{
    size_t index=SizeClass::Index(size);
    //对桶上锁
    _spanList[index]._mtx.lock();
    Span* span= GetOneSpan(_spanList[index],size);
    assert(span);
    assert(span->_freeList);
    start=span->_freeList;
    end=start;
    size_t actnum=1;
    //start和end实在获取大小的范围内
    //左闭右闭
    for(size_t i=0;i<batchNum-1&& NextObj(end)!= nullptr;++i)
    {
        end= NextObj(end);
        ++actnum;
    }
    span->_freeList= NextObj(end);
    NextObj(end)= nullptr;
    span->_useCount+=actnum;
    _spanList[index]._mtx.unlock();
    return actnum;
}
void CentralCache::ReleaseListToSpans(void* start,size_t size)
{
    size_t index=SizeClass::Index(size);
    _spanList[index]._mtx.lock();
    while(start)
    {
        void* next= NextObj(start);
        Span* span=PageCache::GetInstance()->MapObjectToSpan(start);
        NextObj(start)=span->_freeList;
        span->_freeList=start;
        --span->_useCount;
        //如果span的小块内存全部还回来了，就把span还给pagecache
        if(span->_useCount==0)
        {
            _spanList[index].Erase(span);
            span->_next= nullptr;
            span->_prev= nullptr;
            span->_freeList= nullptr;
            _spanList[index]._mtx.unlock();

            PageCache::GetInstance()->_pageMtx.lock();
            PageCache::GetInstance()->ReleaseSpanToPageCache(span);
            PageCache::GetInstance()->_pageMtx.unlock();

            _spanList[index]._mtx.lock();
        }
        start=next;
    }
    _spanList[index]._mtx.unlock();
}