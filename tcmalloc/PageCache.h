//
// Created by 王子豪 on 2023/6/7.
//

#ifndef TCMALLOC_PAGECACHE_H
#define TCMALLOC_PAGECACHE_H

#endif //TCMALLOC_PAGECACHE_H
#pragma once
#include "objectPool.h"
#include<unordered_map>
#include "PageMap.h"
#include "Common.h"
class PageCache
{
public:
    //单例模式
    static PageCache* GetInstance()
    {
        return &_sInst;
    }
    //获取一个k页的span
    Span* NewSpan(size_t k);
    //获取从对象到span的映射
    Span* MapObjectToSpan(void* ptr);
    //释放空闲内存到pagecache,合并相邻的span
    void ReleaseSpanToPageCache(Span* span);
    std::mutex _pageMtx;
    objectPool<Span> _spanPool;

private:
    SpanList _spanLists[NPAGES];
    PageCache() {}
    PageCache(const PageCache&)=delete;
    static PageCache _sInst;
//    std::unordered_map<PAGE_ID,Span*> _Mapid;
    TCMalloc_PageMap1<32-PAGE_SHIFT> _Mapid;
};