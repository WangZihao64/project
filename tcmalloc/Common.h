//
// Created by 王子豪 on 2023/5/19.
//

#ifndef TCMALLOC_COMMON_H
#define TCMALLOC_COMMON_H

#endif //TCMALLOC_COMMON_H
#pragma once
#include<iostream>
#include<assert.h>
#include<mutex>
#ifdef _WIN32
#include<Windows.h>
#else
#include <unistd.h>
#endif
#include<sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
//跨平台
#ifdef _WIN32
    typedef size_t PAGE_ID;
#elif _WIN64
    typedef unsigned long long PAGE_ID;
#else
    typedef unsigned long long PAGE_ID;
#endif
static const size_t MAX_BYTES=256*1024;
static const size_t NFREELIST=208;
static const size_t NPAGES=129;
static const size_t PAGE_SHIFT = 13;
inline static void* SystemAlloc(size_t kpage)
{
#ifdef _WIN32
    void* ptr = VirtualAlloc(0, kpage << 13, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
//linux
#else
// 错误写法*****
//    void* ptr = mmap(NULL,kpage,PROT_READ|PROT_WRITE|PROT_EXEC,MAP_SHARED,-1,0);
//    int fd = open("hello244", O_RDWR|O_CREAT|O_TRUNC, 0644);
//    if (fd < 0){
//        std::cout << "open error" << std::endl;
//    }
//    void* ptr=mmap(NULL, kpage<<13, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
//*****
    //申请128*8KB内存
    void* ptr=sbrk(kpage<<13);
#endif
    if(ptr==MAP_FAILED)
    {
        std::cout << "mmap fail" << std::endl;
        throw std::bad_alloc();
    }
    return ptr;
}
inline static void SystemFree(void* ptr){
#ifdef _WIN32
    VirtualFree(ptr, 0, MEM_RELEASE);
#else
    brk(ptr);
#endif
}
static void*& NextObj(void* obj)
{
    return *(void**)obj;
}
class FreeList
{
public:
//    FreeList()
//    :_freelist(nullptr)
//    {}
    void push(void* obj)
    {
        assert(obj);
        NextObj(obj)=_freelist;
        _freelist=obj;
        ++_size;
    }
    void pushRange(void* start,void* end,size_t n)
    {
        NextObj(end)=_freelist;
        _freelist=start;
        _size+=n;
    }
    void PopRange(void*& start,void*& end,size_t n)
    {
        assert(n<=_size);
        start=_freelist;
        end=_freelist;
        for(int i=0;i<n-1;++i)
        {
            end= NextObj(end);
        }
        _freelist= NextObj(end);
        NextObj(end)= nullptr;
        _size-=n;
    }
    void* pop()
    {
        assert(_freelist);
        //头删
        void* obj=_freelist;
        _freelist=NextObj(obj);
        --_size;
        return obj;
    }
    bool empty()
    {
        return _freelist== nullptr;
    }
    size_t& maxsize()
    {
        return _maxsize;
    }
    size_t Size()
    {
        return _size;
    }
private:
    void* _freelist= nullptr;
    size_t _maxsize=1;
    size_t _size=0;
};
//计算对象大小的对齐映射规则
class SizeClass
{
    // [1,128]					8byte对齐	    freelist[0,16)
    // [128+1,1024]				16byte对齐	    freelist[16,72)
    // [1024+1,8*1024]			128byte对齐	    freelist[72,128)
    // [8*1024+1,64*1024]		1024byte对齐     freelist[128,184)
    // [64*1024+1,256*1024]		8*1024byte对齐   freelist[184,208)
public:
    //普通写法
//    static inline size_t _RoundUp(size_t byte,size_t align)
//    {
//        size_t res=0;
//        if(byte%align==0)
//        {
//            res=byte/align;
//        }
//        else
//        {
//            res=(byte/align+1)*align;
//        }
//        return res;
//    }
    static inline size_t _RoundUp(size_t byte,size_t align)
    {
        return ((byte+align-1))&~(align-1);
    }
    static inline size_t RoundUp(size_t size)
    {
        if(size<=128)
        {
            return _RoundUp(size,8);
        }
        else if(size<=1024)
        {
            return _RoundUp(size,16);
        }
        else if(size<=8*1024)
        {
            return _RoundUp(size,128);
        }
        else if(size<=64*1024)
        {
            return _RoundUp(size,1024);
        }
        else if(size<=256*1024)
        {
            return _RoundUp(size,8*1024);
        }
        else
        {
            return _RoundUp(size,1<<PAGE_SHIFT);
        }
    }
    //普通写法
//    size_t _Index(size_t byte,size_t align)
//    {
//        if(byte%align==0)
//        {
//            return byte/align-1;
//        }
//        return byte/align;
//    }
    static inline size_t _Index(size_t bytes,size_t align_shift)
    {
        return ((bytes + (1 << align_shift) - 1) >> align_shift) - 1;
    }
    //计算是映射哪一个自由链表桶
    static inline size_t Index(size_t size)
    {
        assert(size<=MAX_BYTES);
        static int grounp_array[4]={16,56,56,56};
        if(size<=128)
        {
            return _Index(size,3);
        }
        else if(size<=1024)
        {
            return _Index(size-128,4)+grounp_array[0];
        }
        else if(size<=8*1024)
        {
            return _Index(size-1024,7)+grounp_array[0]+grounp_array[1];
        }
        else if(size<=64*1024)
        {
            return _Index(size-8*1024,10)+grounp_array[0]+grounp_array[1]+grounp_array[2];
        }
        else if(size<=256*1024)
        {
            return _Index(size-64*1024,13)+grounp_array[0]+grounp_array[1]+grounp_array[2]+grounp_array[3];
        }
        else
        {
            assert(false);
        }
        return -1;
    }
    static size_t NumMoveSize(size_t size)
    {
        assert(size > 0);

        // [2, 512]，一次批量移动多少个对象的(慢启动)上限值
        // 小对象一次批量上限高
        // 小对象一次批量上限低
        int num = MAX_BYTES / size;
        if (num < 2)
            num = 2;

        if (num > 512)
            num = 512;

        return num;
    }
    static size_t NumMovePage(size_t size)
    {
        size_t num = NumMoveSize(size);
        size_t npage = num*size;

        npage >>= PAGE_SHIFT;
        if (npage == 0)
            npage = 1;

        return npage;
    }
};
//管理多个连续页大块内存跨度结构
struct Span
{
    //大块内存起始页的页号
    PAGE_ID _pageId=0;
    //页的数量
    size_t _n=0;
    Span* _next= nullptr;
    Span* _prev= nullptr;
    //切好小块内存，被分配给thread cache的计数
    size_t _useCount=0;
    void* _freeList= nullptr; //切好的小块内存的自由链表
    bool _isUse=false; //是否在被使用
    size_t _objSize=0; //切好的小对象的大小
};
class SpanList
{
public:
    SpanList()
    {
        _head=new Span;
        _head->_prev=_head;
        _head->_next=_head;
    }
    Span* Begin()
    {
        return _head->_next;
    }
    Span* End()
    {
        return _head;
    }
    void PushFront(Span* span)
    {
        Insert(Begin(),span);
    }
    Span* PopFront()
    {
        Span* front=_head->_next;
        Erase(front);
        return front;
    }
    bool empty()
    {
        return _head->_next==_head;
    }
    //pos之前插入
    //prev newSpan pos
    void Insert(Span* pos,Span* newSpan)
    {
        assert(pos);
        assert(newSpan);
        Span* prev=pos->_prev;
        newSpan->_prev=prev;
        prev->_next=newSpan;
        newSpan->_next=pos;
        pos->_prev=newSpan;
    }
    //删除当前位置
    void Erase(Span* pos)
    {
        assert(pos);

        assert(pos!=_head);
        Span *prev = pos->_prev;
        Span *next = pos->_next;
        prev->_next = next;
        next->_prev = prev;

    }
private:
    Span* _head;
public:
    std::mutex _mtx; //桶锁
};


