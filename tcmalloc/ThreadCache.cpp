//
// Created by 王子豪 on 2023/5/19.
//

#include <algorithm>
#include "CentralCache.h"
#include "ThreadCache.h"
CentralCache CentralCache::_sInt;
using namespace std;

void* ThreadCache::Allocate(size_t size)
{
    assert(size<=MAX_BYTES);
    //找到映射的桶
    size_t index=SizeClass::Index(size);
    //获得大小
    size_t bytes=SizeClass::RoundUp(size);
    if(!_freelist[index].empty())
    {
        return _freelist[index].pop();
    }
    //使用central cache
    else
    {
        return FetchFromCentralCache(index,bytes);
    }
}
void ThreadCache::Deallocate(void *ptr, size_t size)
{
    assert(ptr);
    assert(size<=MAX_BYTES);
    size_t index=SizeClass::Index(size);
    _freelist[index].push(ptr);
    //当链表长度大于一次批量申请的长度就归还给central cache
    if(_freelist[index].Size()>=_freelist[index].maxsize())
    {
        ListTooLong(_freelist[index],size);
    }
}
void ThreadCache::ListTooLong(FreeList& list,size_t size)
{
    void* start= nullptr;
    void* end= nullptr;
    list.PopRange(start,end,list.maxsize());
    CentralCache::GetInstance()->ReleaseListToSpans(start,size);
}
void* ThreadCache::FetchFromCentralCache(size_t index,size_t bytes)
{
    //慢开始调节算法
    //要的内存大小越大给的越少，要的内存大小越小给的越多
    size_t batchnum=min(_freelist[index].maxsize(),SizeClass::NumMoveSize(bytes));
    if(batchnum==_freelist[index].maxsize())
    {
        _freelist[index].maxsize()+=1;
    }
    void* start= nullptr;
    void* end= nullptr;
    //从central cache中获取对象
    size_t actnum=CentralCache::GetInstance()->FetchRangeObj(start,end,batchnum,bytes);
    assert(actnum>0);
    //实际获得的大小是1直接返回start，如果大于1则链接到freelist上，返回start
    if(actnum==1)
    {
        assert(start==end);
        return start;
    }
    else
    {
        _freelist[index].pushRange(NextObj(start),end,actnum-1);
        return start;
    }
}