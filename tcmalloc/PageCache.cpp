//
// Created by 王子豪 on 2023/6/7.
//
#include "PageCache.h"
#include <memory>

PageCache PageCache::_sInst;
Span* PageCache::NewSpan(size_t k)
{
    assert(k>0);
    //大于128页，向堆申请
    if(k>NPAGES-1)
    {
        void* ptr= SystemAlloc(k);
        Span* span=_spanPool.New();
//        Span* span=new Span;
        span->_pageId=(PAGE_ID)ptr>>PAGE_SHIFT;
        span->_n=k;
        _Mapid.set(span->_pageId,span);
//        _Mapid[span->_pageId]=span;
        return span;
    }
    //第k个桶里面有span
    if(!_spanLists[k].empty())
    {
        Span* kspan=_spanLists[k].PopFront();
        //建立id和span的映射
        for(int i=0;i<kspan->_n;++i)
        {
            _Mapid.set(kspan->_pageId+i,kspan);
//            _Mapid[kspan->_pageId+i]=kspan;
        }
        return kspan;
    }
    //如果第k个桶没有span，就会往下找
    //把第k个桶分为k个和n-k个
    for(int i=k+1;i<NPAGES;++i)
    {
        if(!_spanLists[i].empty())
        {
            //在nspan的头部切下一个k页下来
            //k页被返回，n页插入到对应的位置
            Span* nspan=_spanLists[i].PopFront();
            Span* kspan=_spanPool.New();
//            Span* kspan=new Span;
            kspan->_pageId=nspan->_pageId;
            kspan->_n=k;
            //Id加
            nspan->_pageId+=k;
            //nspan的页数量需要-k
            nspan->_n-=k;
            _spanLists[nspan->_n].PushFront(nspan);
            //存储nspan的页号和映射
            //建立id和span的映射，方便central cache回收小块内存时，查找对应的span
            _Mapid.set(nspan->_pageId,nspan);
            _Mapid.set(nspan->_pageId+nspan->_n-1,nspan);
//            _Mapid[nspan->_pageId]=nspan;
//            _Mapid[nspan->_pageId+nspan->_n-1]=nspan;

            //建立id和span的映射
            for(int i=0;i<kspan->_n;++i)
            {
                _Mapid.set(kspan->_pageId+i,kspan);
//                _Mapid[kspan->_pageId+i]=kspan;
            }
            return kspan;
        }
    }
    //走到这里就没有更大的span了
    //这时就需要堆找一个128页的span
    Span* bigspan=_spanPool.New();
//    Span* bigspan=new Span;
    void* ptr= SystemAlloc(NPAGES-1);
    bigspan->_n=NPAGES-1;
    bigspan->_pageId=(PAGE_ID)ptr>>PAGE_SHIFT;
    _spanLists[bigspan->_n].PushFront(bigspan);
    return NewSpan(k);
}
Span* PageCache::MapObjectToSpan(void* ptr)
{
    PAGE_ID id=((PAGE_ID)ptr>>PAGE_SHIFT);
    auto ret=(Span*)_Mapid.get(id);
    assert(ret!= nullptr);
    return ret;
    //RAII的加锁
//    std::unique_lock<std::mutex> lock(_pageMtx);

//    auto ret=_Mapid.find(id);
//    if(ret!=_Mapid.end())
//    {
//        return ret->second;
//    }
//    else
//    {
//        assert(false);
//        return nullptr;
//    }
}
void PageCache::ReleaseSpanToPageCache(Span* span)
{
    //直接释放
    if(span->_n>NPAGES-1)
    {
        void* ptr=(void*)(span->_pageId<<PAGE_SHIFT);
        SystemFree(ptr);
        _spanPool.Del(span);
//        delete span;
        return ;
    }
    //对前页进行合并
    while(1)
    {
        PAGE_ID prevspan=span->_pageId-1;
        auto ret=(Span*)_Mapid.get(prevspan);
//        auto ret=_Mapid.find(prevspan);
        //没找到
        if(ret== nullptr)
        {
            break;
        }
        //正在被使用
        if(ret->_isUse== true)
        {
            break;
        }
        //大于128页
        if(ret->_n+span->_n>NPAGES-1)
        {
            break;
        }
        span->_pageId=ret->_pageId;
        span->_n+=ret->_n;
        //从之前的桶去掉
        _spanLists[ret->_n].Erase(ret);
        //kspan是new出来的
        _spanPool.Del(ret);
//        delete ret->second;
    }
    //向后合并
    while(1)
    {
        PAGE_ID nextspan=span->_pageId+span->_n;
        auto ret=(Span*)_Mapid.get(nextspan);
        //没找到
        if(ret== nullptr)
        {
            break;
        }
        //正在被使用
        if(ret->_isUse== true)
        {
            break;
        }
        //大于128页
        if(ret->_n+span->_n>NPAGES-1)
        {
            break;
        }
        span->_pageId=ret->_pageId;
        span->_n+=ret->_n;
        //kspan是new出来的
        _spanLists[ret->_n].Erase(ret);
        _spanPool.Del(ret);
//        delete ret->second;
    }
    _spanLists[span->_n].PushFront(span);
}