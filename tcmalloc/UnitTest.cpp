//
// Created by 王子豪 on 2023/5/20.
//
#include "objectPool.h"
#include "ConcurrentAlloc.h"
#include<thread>
using namespace std;
void Alloc1()
{
    for (size_t i = 0; i < 5; ++i)
    {
        void* ptr = ConcurrentAlloc(6);
    }
}

void Alloc2()
{
    for (size_t i = 0; i < 5; ++i)
    {
        void* ptr = ConcurrentAlloc(7);
    }
}

void TLSTest()
{
    std::thread t1(Alloc1);
    t1.join();

    std::thread t2(Alloc2);
    t2.join();
}

void TestConcurrentAlloc1()
{
    void* p1 = ConcurrentAlloc(6);
    void* p2 = ConcurrentAlloc(8);
    void* p3 = ConcurrentAlloc(1);
    void* p4 = ConcurrentAlloc(7);
    void* p5 = ConcurrentAlloc(8);

    cout << p1 << endl;
    cout << p2 << endl;
    cout << p3 << endl;
    cout << p4 << endl;
    cout << p5 << endl;
}

void MultiThreadAlloc1()
{
    std::vector<void*> v;
    for (size_t i = 0; i < 7; ++i)
    {
        void* ptr = ConcurrentAlloc(6);
        v.push_back(ptr);
    }

    for (auto e : v)
    {
        ConcurrentFree(e);
    }
}

void MultiThreadAlloc2()
{
    std::vector<void*> v;
    for (size_t i = 0; i < 7; ++i)
    {
        void* ptr = ConcurrentAlloc(16);
        v.push_back(ptr);
    }

    for (auto e : v)
    {
        ConcurrentFree(e);
    }
}

void TestMultiThread()
{
    std::thread t1(MultiThreadAlloc1);
    std::thread t2(MultiThreadAlloc2);

    t1.join();
    t2.join();
}
void TestFree()
{
    void* p1 = ConcurrentAlloc(9);
    void* p2 = ConcurrentAlloc(8);
    void* p3 = ConcurrentAlloc(1);
    void* p4 = ConcurrentAlloc(7);
    void* p5 = ConcurrentAlloc(8);
    void* p6 = ConcurrentAlloc(8);
    void* p7 = ConcurrentAlloc(8);
    ConcurrentFree(p1);
    ConcurrentFree(p2);
    ConcurrentFree(p3);
    ConcurrentFree(p4);
    ConcurrentFree(p5);
    ConcurrentFree(p6);
    ConcurrentFree(p7);
}
int main()
{
//    TestFree();
//    TestMultiThread();
    //TestObjectPool();
    //TLSTest();

//    TestConcurrentAlloc1();

    return 0;
}