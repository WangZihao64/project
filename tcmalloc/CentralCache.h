//
// Created by 王子豪 on 2023/5/23.
//

#ifndef TCMALLOC_CENTRALCACHE_H
#define TCMALLOC_CENTRALCACHE_H

#endif //TCMALLOC_CENTRALCACHE_H
#pragma once
#include "Common.h"
//单例模式 恶汉模式
class CentralCache
{
public:
    static CentralCache* GetInstance()
    {
        return &_sInt;
    }
    //获取一个非空的Span
    Span* GetOneSpan(SpanList& list,size_t byte_size);
    //从中心缓存获取一定数量的对象给thread cache
    size_t FetchRangeObj(void*& start,void*& end,size_t batchNum,size_t size);
    void ReleaseListToSpans(void* start,size_t byte_size);
private:
    CentralCache(){}
    CentralCache(const CentralCache&)=delete;
    SpanList _spanList[NFREELIST];
    static CentralCache _sInt;
};
//CentralCache CentralCache::_sInt;

