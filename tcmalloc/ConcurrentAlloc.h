//
// Created by 王子豪 on 2023/5/20.
//

#ifndef TCMALLOC_CONCURRENTALLOC_H
#define TCMALLOC_CONCURRENTALLOC_H

#endif //TCMALLOC_CONCURRENTALLOC_H
#pragma once
#include "ThreadCache.h"
#include "Common.h"
#include "PageCache.h"
#include "objectPool.h"
static void* ConcurrentAlloc(size_t size)
{
    //大于256kb直接找page cache
    //大于256kb小于128页的可以利用page cache合并去解决外碎片的问题
    //大于128页的不可以
    if(size>MAX_BYTES)
    {
        //对其规则
        size_t alignsize=SizeClass::RoundUp(size);
        //获得页号
        size_t kpage=alignsize>>PAGE_SHIFT;
        PageCache::GetInstance()->_pageMtx.lock();
        Span* span=PageCache::GetInstance()->NewSpan(kpage);
        span->_objSize=size;
        PageCache::GetInstance()->_pageMtx.unlock();
        void* ptr=(void*)(span->_pageId<<PAGE_SHIFT);
        return ptr;
    }
    else
    {
        if(pTlsThreadCache== nullptr)
        {
            static objectPool<ThreadCache> tcPool;
//            pTlsThreadCache=tcPool.New();
            pTlsThreadCache=new ThreadCache;
        }
//        std::cout << pTlsThreadCache << "       " << std::this_thread::get_id() << std::endl;
        return pTlsThreadCache->Allocate(size);
    }
}
static void ConcurrentFree(void* ptr)
{
    Span* span=PageCache::GetInstance()->MapObjectToSpan(ptr);
    size_t size=span->_objSize;
    if(size>MAX_BYTES)
    {
        PageCache::GetInstance()->_pageMtx.lock();
        PageCache::GetInstance()->ReleaseSpanToPageCache(span);
        PageCache::GetInstance()->_pageMtx.unlock();
    }
    else
    {
        assert(pTlsThreadCache);
        pTlsThreadCache->Deallocate(ptr,size);
    }
}
